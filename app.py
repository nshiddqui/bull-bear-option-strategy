from argparse import ArgumentParser
from datetime import datetime
from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request
from flask import session
import json
import requests
import time


app = Flask(__name__, template_folder='./')
app.secret_key = "87503O003d95U44369G3658b42A31288831d6lu94o60803P855"
file_name = 'latest_data.json';


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/get-data', strict_slashes=False, methods=['GET'])
def getData():
    url = "https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY"
    headers = {
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36'
    }
    response = requests.request("GET", url, headers=headers)
    if is_json(response.text) is False:
        with open(file_name) as json_file:
            return json.loads(json_file)
    return json.loads(response.text)

def is_json(myjson):
    try:
        json_object = json.loads(myjson)
        with open(file_name, 'w') as outfile:
            json.dump(json_object, outfile)
    except e:
        return False
    return True


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=9000, debug=True)
